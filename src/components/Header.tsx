import React from "react";
interface Props {
    title: string
}

const Header = ({ title }: Props) => {
    return (
        <div className="mx-auto mx-w-screen-xl p-4 py-8" >
            <span className="font-extrabold text-transparent text-4xl bg-clip-text bg-gradient-to-r from-purple-400 to-purple-600">
                {title}
            </span>    
        </div>
    )
}

export default Header